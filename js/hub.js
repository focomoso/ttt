'use strict';
/**
* controller for the hub page for the tic tac toe game:
*/


angular.module("TTTApp").controller("hubCtrl", hubCtrl);

hubCtrl.$inject = ['$firebase','$state','Tokens','GameMaker' ];
function hubCtrl($firebase, $state, Tokens, GameMaker){
  // states:
  //  needName      - need to create player
  //  open          - ready for challenges
  //  message       - show a message
  //  challenging   - challenge sent to other player
  //  challenged    - other player has sent a challenge
  //  accepted      - challenge has been accepted by other player
  //  playing       - off playing the game

  var _this = this;

  this.message = "Pick a name to play.";

  this.tokens = Tokens.tokens; // list of game tokens from Token factory 

  this.inputName = "";
  this.inputNameGood = false;
  this.inputNameMsg = " is free!";

  this.chatBox = "";

  this.players = getPlayers();
  this.chats = getChats();

  // if GameMaker.player is defined, we're coming back to this page after playing
  if (GameMaker.player) {

    _this.player = GameMaker.player;
    _this.player.token = {};
    _this.player.state = "open";
    _this.player.challengedBy = "";
    _this.players.$save(_this.player);    

  } else {

    _this.player = {};
    _this.player.name = "";
    _this.player.wins = 0;
    _this.player.state = "needName";
    _this.player.challengedBy = "";
    _this.player.token = {};

  }
  
  // connections to firebase
  function getPlayers() {
  	var ref = new Firebase("https://focottt.firebaseio.com/players");
    return $firebase(ref).$asArray();
  }
  function getChats() {
    var ref = new Firebase("https://focottt.firebaseio.com/chats").orderByChild("timeStamp").limitToLast(100);
    //var query = ref.orderByChild("timeStamp").limitToLast(10);

    return $firebase(ref).$asArray();
  }

// event handlers

  // check whether the input name is avaliable while player types
  this.inputNameChange = function(){
    
    // test whether the input name is free
    var nameExists = false;
    for (var i in _this.players)
      if (_this.inputName == _this.players[i].name) {
        nameExists = true;
        break;
      }

    // write appropriate message
    if (nameExists) {

      // name is already in use
      _this.inputNameGood = false;
      _this.inputNameMsg = " is taken!";
    } else {

      // name is good
      _this.inputNameGood = true;
      _this.inputNameMsg = " is free!";
    }
  }; // inputNameChange

  // player submits a name
  this.inputNameSubmit = function(){

    // if name is unique
    if (_this.inputNameGood) {
      
      // add the name to firebase
      _this.players.$add({
          name: _this.inputName,
          state: "open",
          wins:0,
          challengedBy: "",
          token: ""
        })
        .then(function (ref) {

          // get the unique key from firebase
          var playerId = ref.key();

          // find and save this player's object from the playerNames
          for (var i in _this.players)
            if (playerId == _this.players[i].$id) {
              _this.player = _this.players[i];
              break;
            }

          // tell firebase to delete this player's record onDiconnect 
          var disconnectRef = new Firebase('https://focottt.firebaseio.com/players/' + playerId);
          disconnectRef.onDisconnect().remove();

          // clear the input field
          _this.inputName = "";
          _this.message = "Chat away or choose someone in green to play with.";
          _this.player.state = "message";
        }
      );
    } // end if
  }; // inputNameSubmit

  this.chatSubmit = function() {
    //console.log("chatting");
    _this.chats.$add({
      name: _this.player.name,
      body: _this.chatBox,
      timeStamp: Firebase.ServerValue.TIMESTAMP
    });
    _this.chatBox = "";
  };

  // player picks someone to play with
  this.hitPlayer = function(playerClicked) {

    // if the Player isn't playing and hasn't been challenged by someone else
    if (!_this.player.name) {

      _this.message = "Please pick a name first."
      _this.player.state = "message";
    
    } else if (playerClicked.state == "challenging") {
    
      _this.message = playerClicked.name + " has challenged someone else.";
      _this.player.state = "message";
    
    } else if (playerClicked.state == "challenged") {
    
      _this.message = playerClicked.name + " has been challenged by someone else.";
      _this.player.state = "message";
    
    } else if (playerClicked.state == "playing") {
    
      _this.message = playerClicked.name + " is already playing.";
      _this.player.state = "message";
    
    } else {
      // check if we're already challenging someone
      if (_this.otherPlayer) {
        // release them if so
        _this.otherPlayer.state = "open";
        _this.otherPlayer.challengedBy = "";
        _this.players.$save(_this.otherPlayer); 
      }

      // challenge other player
      
      _this.player.state = "challenging";
      playerClicked.state = "challenged";
      playerClicked.challengedBy = _this.player.name;
      _this.otherPlayer = playerClicked;

      _this.players.$save(playerClicked); 
    }
    _this.players.$save(_this.player); 
  }; // hitPlayer

  // player accepts challenge 
  this.acceptChallenge = function() {
    
    _this.otherPlayer = getOtherPlayer();

    _this.player.state = "accepted";
    _this.otherPlayer.state = "accepted";
    _this.players.$save(_this.player);
    _this.players.$save(_this.otherPlayer); 
  };

  // player rejects challenge
  this.rejectChallenge = function() {

    _this.otherPlayer = getOtherPlayer();

    if (_this.otherPlayer) {

      _this.otherPlayer.state = "open";
      _this.players.$save(_this.otherPlayer); 
    }

    _this.player.challengedBy = "";
    _this.player.state = "open";
    _this.players.$save(_this.player);
    
  };  

  // player picks a token
  this.hitToken = function(token) {
    _this.player.token = token.display;
    _this.players.$save(_this.player);

    GameMaker.setPlayer(_this.players, _this.player, _this.otherPlayer);

    // test whether the other player has picked
    if (_this.otherPlayer.token.length > 0) {
      
      // we're the last one to pick so...

      // make other player player 0
      _this.player.playerNum = 1;
      _this.otherPlayer.playerNum = 0;
      _this.players.$save(_this.player);
      _this.players.$save(_this.otherPlayer);
      
      // set up the game board (using the GameMaker factory)
      GameMaker.make();
    }
  };

  this.play2d = function() {
    _this.player.state = "playing";
    _this.players.$save(_this.player);

    $state.go('ttt2d'); 
  };

  this.play3d = function() {
    //$state.go('ttt3d');
  };

  var getOtherPlayer = function() {

    if (_this.player.challengedBy.length > 0) {
    
      for (var i=0;i<_this.players.length;i++) {
    
        if (_this.players[i].name == _this.player.challengedBy) {
          return _this.players[i];
        }
      }
    }
    return undefined;
  };
} // end ctrl




