'use strict';

angular.module("TTTApp").controller("TTT2dCtrl", TTT2dCtrl);

TTT2dCtrl.$inject = ['$firebase','$state','GameMaker'];
function TTT2dCtrl( $firebase, $state, GameMaker ){
  var _this = this;
  
  if (!GameMaker.player) {
    $state.go('home'); 
    return;
  }  
  _this.player = GameMaker.player;
  _this.otherPlayer = GameMaker.otherPlayer;
  _this.players = GameMaker.players;


  _this.game = getGame();
  _this.resetGame = resetGame;

  _this.player.wins ? null : _this.player.wins = 0;
  _this.players.$save(_this.player);


  // connect to specific firebase game 
  function getGame(){
    var ref = new Firebase("https://focottt.firebaseio.com/games/" + _this.player.gameId);
    return $firebase(ref).$asObject();
  }

  // returns the player whose turn it is (or winner if game's over)
  this.getCurrPlayer = function(){
    return _this.game.toPlay == _this.player.playerNum ? _this.player : _this.otherPlayer;
  };

  // called by the view when the board is played
  this.cellHit = function(idx){
    if (  !_this.game.over &&                             // game's not over
          _this.game.board[idx] == "" &&                  // and the cell is empty
          _this.player.playerNum == _this.game.toPlay) {  // and it's our turn 

      _this.game.board[idx] = _this.player.token;
  
      if ( checkForWinner(_this.player.token) ) {
        _this.game.over = true;
        _this.game.overMessage = _this.player.name + " WINS!";
        _this.player.wins++;
        _this.players.$save(_this.player);
      } else {
        // only check for cats game if no winner
        if ( checkCatsGame() ) {
          _this.game.over = true;
          _this.game.overMessage = "No winner.";
        }
      }
      _this.game.toPlay = !_this.game.toPlay;
      _this.game.$save();
    }  
  };


  // resets the game
  function resetGame(){
    _this.game.board = ["","","","","","","","",""];    
    _this.game.over = false;
    _this.game.toPlay = 0; 
    _this.game.$save();
  }

  
  // checks whether x or o won
  function checkForWinner(p) {
    var gb = _this.game.board;

    if (
        (gb[0]==p && gb[1]==p && gb[2]==p) ||
        (gb[3]==p && gb[4]==p && gb[5]==p) ||
        (gb[6]==p && gb[7]==p && gb[8]==p) ||
        (gb[0]==p && gb[3]==p && gb[6]==p) ||
        (gb[1]==p && gb[4]==p && gb[7]==p) ||
        (gb[2]==p && gb[5]==p && gb[8]==p) ||
        (gb[0]==p && gb[4]==p && gb[8]==p) ||
        (gb[2]==p && gb[4]==p && gb[6]==p)
      ){
      return true;
    }
    return false;
  }

  // returns true if all the boxes are filled
  function checkCatsGame() {

    var gb = _this.game.board;
    var done = true;
    for (var i in gb) {
      if (gb[i]=="")  // if there are any empty cells, then it's not a cat's game
        return false;
    }
    return true;
  }
}













