'use strict';

angular.module("TTTApp", ['firebase','ui.router'])
  .config(uiConfig)
  .factory('Tokens',TokenLoader)
  .factory('GameMaker',GameMaker);

/**
* config our states for the app
*/
// later use locationProvider to getpretty urls (but not for testing because you can't refresh)
// .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
function uiConfig($stateProvider, $urlRouterProvider){
  $urlRouterProvider.otherwise('/home');

  $stateProvider    
    .state('home', {
      url: '/home',
      templateUrl: '/partials/hub.html',
      controller: 'hubCtrl as hub'
    })
    .state('ttt2d', {
      url: '/ttt2d/',
      templateUrl: '/partials/ttt2d.html',
      controller: 'TTT2dCtrl as ttt'
    })    
    .state('ttt3d', {
      url: '/ttt3d/:slots',
      templateUrl: '/partials/ttt3d.html',
      controller: 'TTT3dCtrl as ttt'
    });

    //$locationProvider.html5Mode(true);
}

/**
* loads 3d tokens via http and json... 
*/
//TokenLoader.$inject = ["$http","$firebase"];
function TokenLoader($http,$firebase){
  var Tokens = {};
  // todo: get objects from firebase and load urls to the 3d json objects via http
  Tokens.tokens = [
    {id: 0,display:"X",src:""},
    {id: 1,display:"O",src:""},
    {id: 2,display:"|",src:""},
    {id: 3,display:"-",src:""},
    {id: 4,display:"#",src:""}
  ];
  return Tokens;
} // end tokenLoader

/**
* builds a unique game
*/
//GameMaker.$inject = ['$firebase'];
function GameMaker($firebase) {
  var Game = {};

  Game.make = function() {
    $firebase(new Firebase("https://focottt.firebaseio.com/games")).$asArray()
      .$add({
        board: ["","","","","","","","",""],
        toPlay: 0,
        over: false
      }).then(function (ref){
        var gameId = ref.key();

        Game.player.gameId = gameId;
        Game.otherPlayer.gameId = gameId;
        Game.players.$save(Game.player);
        Game.players.$save(Game.otherPlayer);

        // tell firebase to delete this game onDiconnect 
        var disconnectRef = new Firebase('https://focottt.firebaseio.com/games/' + gameId);
        disconnectRef.onDisconnect().remove();
      }
    );
  };

  Game.setPlayer = function(players, player, otherPlayer) {
    Game.players = players; // need the players to keep connection with firebase
    Game.player = player;
    Game.otherPlayer = otherPlayer;
  }

  return Game;
}




// http usage
  // return {
  //   getMembers: function(){

  //     var url = "https://api.github.com/teams/1124902/members?access_token="  
  //     var apiToken = "<YOUR API KEY HERE>";
  //     var endpoint = url + apiToken;

  //     return $http({method: 'GET', url: endpoint});
  //   }
  // };



//http://stackoverflow.com/questions/24575491/angular-ui-router-passing-data-beetween-states-with-go-function-does-not-work
    
